/**
 * Created by akin on 9.11.2015.
 */
define(["./scenegraph"], function (SceneGraph)
{
    class Context
    {
        constructor(settings)
        {
            this.scenegraph = new SceneGraph();
            if(settings !== undefined)
            {
                this.setup(settings);
            }
        }

        setup(settings)
        {
            if(settings === undefined)
            {
                return;
            }
            this.scenegraph.setup(settings.scenegraph);
        }
    }

    return Context;
});
