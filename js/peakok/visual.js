/**
 * Created by akin on 9.11.2015.
 */
define(["three"], function (THREE)
{
    class Visual
    {
        constructor(settings)
        {
            this.mesh = null;
            if(settings !== undefined )
            {
                this.setup(settings);
            }
        }

        setup(settings)
        {
            if(settings.mesh === undefined)
            {
                return;
            }
            this.mesh = settings.mesh;
        }
    }

    return Visual;
});
