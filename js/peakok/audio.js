/**
 * Created by akin on 9.11.2015.
 */
define(function()
{
    class Audio
    {
        constructor(settings)
        {
            this.policy = {
            };

            if(settings !== undefined )
            {
                this.setup(settings);
            }
        }

        setup(settings)
        {
            if(settings === undefined)
            {
                return;
            }
            if(settings instanceof Array)
            {
                var that = this;
                forEach(settings , function(item){
                    that.addPolicy(item);
                });
            }
        }

        addPolicy(item)
        {
            var src = item.source;
            if( src === null || src === undefined ) {
                src = [];
            }
            if(!src instanceof Array) {
                src = [src];
            }

            // Todo Preload src, or at least attack buffers.
            // preferrably all. gui effects shouldnt be enormous.

            this.policy[item.key] = {
                source: src,
                type: item.type,
                flags: item.flags,
                volume: item.volume
            }
        }

        play(key)
        {
            if(this.policy[key] === undefined)
            {
                return;
            }
            // TODO, How exactly are we going to play audio?
            // centralized audio system//singleton? Yes.
        }
    }

    return Audio;
});
