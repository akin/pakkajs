/**
 * Created by akin on 9.11.2015.
 */
define(function()
{
    class SceneGraph
    {
        constructor(settings)
        {
            this.node = new THREE.Object3D();
            if(settings !== undefined )
            {
                this.setup(settings);
            }
        }

        setup(settings)
        {
        }

        add(node)
        {
            if( !node instanceof SceneGraph )
            {
                // TODO Error!
                return;
            }
            this.node.add(node.node);
        }

        enabled(state)
        {
            return this.node.enabled(state);
        }
    }

    return SceneGraph;
});

