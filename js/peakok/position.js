/**
 * Created by akin on 9.11.2015.
 */
define(["three"], function (THREE)
{
    class Position
    {
        constructor(settings)
        {
            this.matrix = new THREE.Matrix4();

            if(settings !== undefined )
            {
                this.setup(settings);
            }
        }

        setup(settings)
        {
            if(settings === undefined)
            {
                return;
            }

            this.matrix = new THREE.Matrix4();
            if(settings.position !== undefined)
            {
                this.matrix.setPosition(settings.position);
            }
            if(settings.rotation !== undefined)
            {
                // TODO!
            }
        }
    }

    return Position;
});
