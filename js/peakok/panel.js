/**
 * Created by akin on 9.11.2015.
 */
define(["./position", "./dimension", "./policy", "./audio", "./scenegraph", "./visual"], function (Position, Dimension, Policy, Audio, SceneGraph, Visual)
{
    class Panel
    {
        constructor(settings)
        {
            this.position = new Position();
            this.dimension = new Dimension();
            this.policy = new Policy();
            this.audio = new Audio();
            this.scenegraph = new SceneGraph();
            this.visual = new Visual();

            if(settings !== undefined)
            {
                this.setup(settings);
            }
        }

        setup(settings)
        {
            if(settings === undefined)
            {
                return;
            }

            this.position.setup(settings.position);
            this.dimension.setup(settings.dimension);
            this.policy.setup(settings.policy);
            this.audio.setup(settings.audio);
            this.scenegraph.setup(settings.scenegraph);
            this.visual.setup(settings.visual);
        }
    }

    return Panel;
});
