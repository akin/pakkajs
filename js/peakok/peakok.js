/**
 * Created by akin on 03/11/15.
 *
 * config = new Peakok({ path: "foo" })
 *                  .set("bar","baz")
 *                  .set("loo","lii")
 *                  .set("lee","loo");
 *
 * .onUpdate(function() {
 * });
 */
define(function () {
    function Peakok(config){
        this.init(config);

        this._name = null;
        this._map = {};
        this._onUpdateArray = [];

        return this;
    }

    Peakok.prototype.destroy = function() {
    }

    Peakok.prototype._onUpdate = function() {
        for(var i = 0 ; i < this._onUpdateArray.length ; ++i) {
            this._onUpdateArray[i]();
        }
    }

    Peakok.prototype.addOnUpdate = function(fn) {
        this._onUpdateArray.push(fn);
    }

    Peakok.prototype.removeOnUpdate = function(fn) {
        for(var i = this._onUpdateArray.length - 1 ; i >= 0 ; --i) {
            if( this._onUpdateArray[i] === fn ) {
                this._onUpdateArray.splice(i , 1);
            }
        }
    }

    Peakok.prototype.set = function(key , val) {
        this._map[key] = val;
        return this;
    }

    Peakok.prototype.get = function(key) {
        if( this._map[key] === undefined ) {
            return undefined;
        }
        return this._map[key];
    }

    Peakok.prototype.init = function(config) {
        this._name = config.name;
        return this;
    }

    return Peakok;
});
