
define(["three", "../system/math", "./widget"], function (THREE, Math, ParentWidget) {
    function Widget(config){
        ParentWidget.call( this , config );
        return this;
    }

    // specify inheritance
    Widget.prototype = Object.create( ParentWidget.prototype );

    Widget.prototype.destroy = function() {
        // Call parent destroy
    }

    Widget.prototype.init = function(config) {
        var image = config.image;
        this.poly = null; //
    }

    return Widget;
});
