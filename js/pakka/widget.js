
define(["three", "../system/math"], function (THREE, Math) {
    function Widget(config){
        this.node = new THREE.Object3D();
        this.parent = null;
        this.init(config);
        return this;
    }

    Widget.prototype.destroy = function() {
        this.detach();
    }

    Widget.prototype.getNode = function() {
        return this.node;
    }

    Widget.prototype.addChild = function(config) {
        if( config.child === null || config.child === undefined ) {
            return;
        }
        config.child.attach({
            parent: this
        });
    }

    Widget.prototype.attach = function(config) {
        if( config.parent === null || config.parent === undefined ) {
            return;
        }
        this.detach();
        this.parent = config.parent;
        this.parent.getNode().add(this.node);
    }

    Widget.prototype.detach = function() {
        if( this.parent != null ) {
            this.parent.getNode().remove(this.node);
            this.parent = null;
        }
    }

    return Widget;
});
