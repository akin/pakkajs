
require(["jquery"], function($) {
    // For each application div, create application, and store the application data into the element itself.
    $("[application]").each(function(index){
        var element = $(this);
        require(["application/" + element.attr("application").toLowerCase()], function(Application){
            element.data("application", new Application({
                element: element
            }));
        });
    });
});
