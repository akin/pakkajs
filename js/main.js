
requirejs.config({
    "baseUrl": "js/jslibs",
    "paths": {
        "app": "../app",
        "jquery": "../vendor/jquery-2.1.4.min",
        "jquery-mobile": "../vendor/jquery.mobile.custom.min",
        "howler": "../vendor/howler.min",
        "three": "../vendor/three.min",
        "tween": "../vendor/tween",
        "common": "../vendor/common"
    },
    priority: ['jquery', 'jquery-mobile'],
    shim: {
        'jquery': {
            deps: []
        },
        'jquery-mobile': {
            deps: ['jquery'],
            exports: 'jquery'
        },
        'three': {
            exports: 'THREE'
        }
    }
});

require(["jquery"], function($) {
    $(document).bind("dragstart drop dragenter dragover dragleave", function(e) {
        e.preventDefault();
        return false;
    });
});
